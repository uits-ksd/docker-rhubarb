#!/bin/bash

# Get current timestamp
timestamp=$(date +"%Y-%m-%d %T")

# Check if sshd is running 
pscmdoutput=$(ps -ef | grep sshd | wc -l)
echo "$timestamp - Health check 'ps -ef | grep sshd | wc -l': output $pscmdoutput" >&1

# Check if we are listening on port 2022
netstatcmdoutput=$(netstat -an | grep 0.0.0.0:2022 | wc -l)
echo "$timestamp - Health check 'netstat -an | grep 0.0.0.0:2022 | wc -l': output $netstatcmdoutput" >&1

# Check if output is correct
if [[ $pscmdoutput -gt 1 && $netstatcmdoutput == 1 ]]; then
    exit 0
else
    exit 1
fi