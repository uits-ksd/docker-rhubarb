#!/bin/bash

# Adapted from cron/templates/s3backup0.erb in the sftphub-puppet code repository and s3backup.sh in the docker-nexus-backup repository.
#
#
# Usage: 
# Intended to be used within a Docker container that has the EFS volume mounted in the container.
#
# Script Parameters:
## CLEANUP_FILE_PATH: local path to files that need to be deleted; for example: /transactional/work/control/history/
## DAYS_TO_KEEP: If provided, will define the max age of the files to keep

# Handle script arguments
DEFAULT_DAYS=7
DEFAULT_PATH=/transactional/work/control/history/
CLEANUP_FILE_PATH=$1
DAYS_TO_KEEP=$2

# Set value for number of days to keep files, if passed in
echo "You provided the arguments:" "$@"

if [ -z $CLEANUP_FILE_PATH ]; then
  echo "No file path supplied; using default of $DEFAULT_PATH"
  CLEANUP_FILE_PATH=$DEFAULT_PATH
fi

if [ -z $DAYS_TO_KEEP ]; then
  echo "No day amount supplied; using default of $DEFAULT_DAYS"
  DAYS_TO_KEEP=$DEFAULT_DAYS
fi

# Record start time to the defined crontab log
echo -en "=============\nStart file cleanup at $(date)\n=============\n"

# Find and delete all files that are older than $DAYS_TO_KEEP days
cd "$CLEANUP_FILE_PATH"
echo -en "\nFiles To Delete:\n"
find "$CLEANUP_FILE_PATH" -maxdepth 1 -type f -mtime +"$DAYS_TO_KEEP" -ls
echo -en "\nNow deleting the files.\n\n"
find "$CLEANUP_FILE_PATH" -maxdepth 1 -type f -mtime +"$DAYS_TO_KEEP" -delete

# Record end time to the log
echo -en "=============\nEnd file cleanup at $(date)\n=============\n"
