#!/bin/bash

echo "Begin rhubarb::setup_transactional_dirs"

ENVIRONMENT_BASE_DIR="/efs/kfs/${LOWER_ENV}"
ln -s "${ENVIRONMENT_BASE_DIR}/transactional" /transactional

echo "End rhubarb::setup_transactional_dirs"