source /opt/tbginc/startup_init.sh
source /usr/local/bin/setup_transactional_dirs.sh
source /usr/local/bin/setup_integration_dirs.sh
source /usr/local/bin/setup_ssh_fingerprints.sh
source /usr/local/bin/install_configs.sh
source /usr/local/bin/export_environment_vars.sh

# as of FIN-3597, this script just creates the /var/log/auth.log file. No longer using rsyslog, iptables, or fail2ban
/usr/local/bin/start-security-apps.sh

# explicitly start sshd so kbatch can log in
/usr/sbin/sshd

# Get hydrated configuration from EC2 host in kuali-configs mapped volume
# this may be obsolete; discovered during FIN-3597 that it wasn't written before release 65 either
cp /rhubarb-security/authinfo /etc/mail/authinfo

# Add authinfo to the authinfo.db
# this may be obsolete; discovered during FIN-3597 that it wasn't written before release 65 either
sudo makemap hash /etc/mail/authinfo.db < /etc/mail/authinfo

# Start sendmail service per AWS support suggestion
source /etc/sysconfig/sendmail
/etc/mail/make
/etc/mail/make aliases
/usr/sbin/sendmail -bd $SENDMAIL_OPTS $SENDMAIL_OPTARG
/bin/touch /run/sm-client.pid
/bin/chown smmsp:smmsp /run/sm-client.pid
/etc/mail/make
/usr/sbin/sendmail -L sm-msp-queue -Ac $SENDMAIL_OPTS $SENDMAIL_OPTARG

# Start cron (as defined in https://bitbucket.org/ua-ecs/docker_dokuwiki/src/uits_docs/startup.sh, for an example)
/usr/sbin/crond -s -n &

tail -f /dev/null