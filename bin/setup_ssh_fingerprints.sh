#!/bin/bash

echo "Begin rhubarb::setup_ssh_fingerprints"

aws s3 cp s3://${AWS_S3_BUCKET}/docker-rhubarb/fingerprints.tar.gz /root/fingerprints.tar.gz

cd /etc/ssh/
tar xvzf /root/fingerprints.tar.gz --strip-components=1
chmod 600 *_key
chmod 644 *_key.pub
chown root:root *_key*

echo "End rhubarb::setup_ssh_fingerprints"