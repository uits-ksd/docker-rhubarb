#!/bin/bash

echo "Begin rhubarb::install_configs"

# Load smtp credentials from secrets manager
ENV_SECRETS=`aws secretsmanager get-secret-value --secret-id "/kfs/${LOWER_ENV}/secrets" --query "SecretString" --output text`
SMTP_USERNAME=`echo ${ENV_SECRETS} | jq -r .smtp_username`
SMTP_PASSWORD=`echo ${ENV_SECRETS} | jq -r .smtp_password`

# SMTP authinfo
SMTP_AUTHINFO="${SMTP_SECURITY_DIRECTORY}/authinfo"
mkdir -p ${SMTP_SECURITY_DIRECTORY}
cp ${TEMPLATES_DIR}/authinfo.tmpl ${SMTP_AUTHINFO}
chmod 644 ${SMTP_AUTHINFO}

sed -i "s|{{ AWS_REGION }}|${AWS_REGION}|g" ${SMTP_AUTHINFO}
sed -i "s|{{ SMTP_USERNAME }}|${SMTP_USERNAME}|g" ${SMTP_AUTHINFO}
sed -i "s|{{ SMTP_PASSWORD }}|${SMTP_PASSWORD}|g" ${SMTP_AUTHINFO}

# Set the SSH port to 2022
echo "Port 2022" >> /etc/ssh/sshd_config

echo "End rhubarb::install_configs"