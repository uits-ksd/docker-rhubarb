#!/bin/bash

source ~/.bashrc &>/dev/null
source /opt/tbginc/startup_init.sh &>/dev/null

#*******************************************************************************
#
# TITLE......: maintenance_mode.sh
# PARAMETER..:
#     INPUTS.: -m = (Required) Mode to toggle. M for maintenance Mode or N for normal
#              -x = (Optional) Add any extra args to the envy maint command. For example "-B" will
#                   bounce the batch stack after putting it in maintenace mode.
#
# --This script requires that the jq package is installed
#   yum install -y jq
#
#*******************************************************************************

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
    cat << EOF
Usage: maintenance_mode.sh -m <m|n> -u <envy_user> -x "-B -l DEBUG"

Global Options:
       -m = (Required) Mode to toggle. M for maintenance Mode or N for normal
       -x = (Optional) Add any extra args to the envy maint command. For example "-B" will
            bounce the batch stack after putting it in maintenace mode.

EOF
    execute_plugin parse_opts_usage
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

# -- Send a message to the #met-errors channel
if ! [ -z "$SLACK_URL" ]; then
  SLACK_MSG="Error in $AWS_ACCOUNT running maintenance_mode.sh - Environment $LOWER_ENV ($MSG)"
  JSON="{ \"channel\": \"$SLACK_ERROR_CHANNEL\", \"text\": \"$SLACK_MSG\" }"
  OUTPUT=$(curl -s -d "payload=$JSON" "$SLACK_ERROR_URL" 2>&1)
fi

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
declare -xu MODE=""
START_TS=`${DATE}`
declare -x STATUS_FILE="/tmp/maint_mode.txt"

# -- parse command-line options
while getopts :m:u:x: arguments
do
  case $arguments in
    m)
       MODE=${OPTARG}
       ;;
    x)
       EXTRA_FLAGS=${OPTARG}
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

echo "User: $ENVY_USER"
echo "Mode: $MODE"
echo "Flags: $EXTRA_FLAGS"
echo "SSM Doc: ${MAINT_MODE_SSM_DOC}"

#Check to see if input is valid
#If mode is not M or N then it is not valid
if [ ! $MODE == "N" ] && [ ! $MODE == "M" ]; then
  error_abort "A valid mode was not passed in. Mode should be either m or n"
fi


STS_SESSION_NAME="MAINTMODE"

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN

echo "Assuming Role: ${MAINT_MODE_ROLE_ARN}"
STS_OUTPUT=$(aws sts assume-role --role-arn ${MAINT_MODE_ROLE_ARN} --role-session-name ${STS_SESSION_NAME} --duration-seconds 14400 --output json)

export AWS_ACCESS_KEY_ID=$(echo $STS_OUTPUT | jq -r .Credentials.AccessKeyId)
export AWS_SECRET_ACCESS_KEY=$(echo $STS_OUTPUT | jq -r .Credentials.SecretAccessKey)
export AWS_SESSION_TOKEN=$(echo $STS_OUTPUT | jq -r .Credentials.SessionToken)

#Repeat this command in the event of a failure, sleep 30 seconds and try again 5 times before error abort
command_cnt="0"
while true
do
  command_cnt=$((command_cnt+1))
  COMMAND_ID=$(aws ssm send-command --document-name "${MAINT_MODE_SSM_DOC}" --targets '{"Key":"tag:Name","Values":["'${ENVY_DEPLOYER}'"]}' --max-concurrency "10" --parameters "{\"env\": [\"$LOWER_ENV\"],\"mode\": [\"$MODE\"], \"user\": [\"$ENVY_USER\"], \"extraFlags\": [\"$EXTRA_FLAGS\"]}" --timeout-seconds 600 --region $AWS_REGION --output text --query "Command.CommandId" 2>&1)
  echo "Kicked off ${MAINT_MODE_SSM_DOC} command COMMAND_ID = ${COMMAND_ID}"
  
  if [ $? != 0 ]; then
    error_abort "Error with asm ssm send-command ($COMMAND_ID)"
  fi
  
  #Loop to check status
  while true
  do
    STATUS=$(aws ssm list-commands --command-id $COMMAND_ID --region $AWS_REGION --output text --query "Commands[*].StatusDetails" 2>&1)
    echo "Checking command STATUS = ${STATUS}"
    if [ $? != 0 ]; then
      error_abort "Error checking status of command ($STATUS)"
    fi
  
    case $STATUS in
      "Pending")
        sleep 5
        ;;
      "InProgress")
        sleep 5
        ;;
      "Success")
        echo "success" > $STATUS_FILE
        break
        ;;
      *)
        echo "error" > $STATUS_FILE
        break
        ;;
    esac
  done
  cmd_status=$(cat $STATUS_FILE)
  #If the cmd status is success break out of the loop
  if [ "$cmd_status" = "success" ]; then
    break;
  #If error then try again up to 5 times after a 30 second sleep
  else
    if [ "$command_cnt" -le "5" ]; then
      sleep 30
    else
      error_abort "5 attempts to modify maintainence mode have failed"
    fi
  fi
done

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN