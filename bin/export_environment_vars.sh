#!/bin/bash

echo "Begin rhubarb::export_environment_vars"

echo "export LOWER_ENV=${LOWER_ENV}" >> /home/pssys/.bashrc
echo "export UPPER_ENV=${UPPER_ENV}" >> /home/pssys/.bashrc
echo "export LOWER_PILLAR=${LOWER_PILLAR}" >> /home/pssys/.bashrc
echo "export UPPER_PILLAR=${UPPER_PILLAR}" >> /home/pssys/.bashrc

echo "END rhubarb::export_environment_vars"
