#!/bin/bash

echo "Begin rhubarb::setup_integration_dirs"

ENVIRONMENT_BASE_DIR="/efs/kfs/${LOWER_ENV}"
ln -s "${ENVIRONMENT_BASE_DIR}/integration" /integration

echo "End rhubarb::setup_integration_dirs"