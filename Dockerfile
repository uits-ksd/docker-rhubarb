# For a Rhubarb container

# Start with our base image from the docker-rhubarb-base repository
ARG DOCKER_REPOSITORY
ARG BASE_DOCKER_TAG_NAME
FROM $DOCKER_REPOSITORY:$BASE_DOCKER_TAG_NAME
LABEL maintainer="U of A Kuali DevOps <katt-support@list.arizona.edu>"

# Build variables
# Default is set to development values (i.e. dev environment and development branch)
ARG TARGET_ENV=dev7
ARG GIT_BRANCH=development

# Add labels to image
LABEL environment=$TARGET_ENV

# Set environment-specific Rhubarb variables
ENV RHUBARB_ENV=$TARGET_ENV
RUN echo 'Set Rhubarb environment to:' $RHUBARB_ENV

# set up default umask for root
RUN echo "umask 002" >> /root/.bashrc

# Set up bash_profile with various environment variables
COPY profiles/kbatch-bash_profile /home/kbatch/.bash_profile
RUN touch /home/kbatch/.bash_profile
# Make sure the value for RHUBARB_ENV makes it into kbatch's environment
RUN echo "export RHUBARB_ENV=$TARGET_ENV" >> /home/kbatch/.bash_profile

# Copy shell scripts, including start script
COPY bin/* /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# set up SSH for kbatch (otherwise connection refused)
# initially from https://bytefreaks.net/gnulinux/centos-6-install-start-and-stop-enable-and-disable-ssh-server
RUN yum install -y openssh-server
# configure ssh-user
RUN sed -i 's/LogLevel INFO/LogLevel VERBOSE/g' /etc/ssh/sshd_config
# secure container following best practices; see https://aws.amazon.com/premiumsupport/knowledge-center/ec2-ssh-best-practices/
RUN sed -i 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
RUN sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
# add ssh-user
RUN useradd ssh-user
# set default environment for ssh-user to bash
RUN usermod -s /bin/bash ssh-user
# set up password-less ssh
RUN ssh-keygen -f /root/.ssh/id_rsa -q -N ""
RUN mkdir -p /home/ssh-user/.ssh
RUN cat /root/.ssh/id_rsa.pub > /home/ssh-user/.ssh/authorized_keys
RUN touch /home/ssh-user/.bash_profile
RUN chown -R ssh-user:ssh-user /home/ssh-user/
# set up target directories for Rhubarb
RUN mkdir /etc/opt/kuali/
RUN chown ssh-user:ssh-user /etc/opt/kuali/
RUN mkdir /opt/kuali/
RUN chown ssh-user:ssh-user /opt/kuali/

# Install bundler so that we have gems that the rhubarb project needs
RUN gem install bundler

# pull rhubarb code based on corresponding $GIT_BRANCH
WORKDIR /opt/kuali/rhubarb/current
RUN git clone 'https://bitbucket.org/uits-ksd/katt-aws-rhubarb' . && \
    git checkout -B $GIT_BRANCH --track origin/$GIT_BRANCH
RUN bundle install

# pull rhubarb config code based on corresponding $GIT_BRANCH
WORKDIR /etc/opt/kuali/rhubarb/current
RUN git clone 'https://bitbucket.org/uits-ksd/katt-aws-rhubarb-config' . && \
    git checkout -B $GIT_BRANCH --track origin/$GIT_BRANCH

# This was moved to a script at runtime. See: setup_ssh_fingerprints.sh
# copy in fingerprints from prototype environment for consistent access by Control-M
# COPY ssh/fingerprints.tar.gz /root/fingerprints.tar.gz
# RUN cd /etc/ssh/ && \
#         tar xvzf /root/fingerprints.tar.gz --strip-components=1 && \
#         chmod 600 *_key && \
#         chmod 644 *_key.pub && \
#         chown root:root *_key*

# reset working directory to default
WORKDIR /root

# Change ownership of Rhubarb directory
RUN chown -R kbatch:kuali /opt/kuali/rhubarb
RUN chown -R kbatch:kuali /etc/opt/kuali/rhubarb

# FIN-155 Install Sendmail Services
# https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-email-sendmail.html

RUN yum install -q -y sendmail sendmail-cf m4

# Append /etc/mail/access file
RUN echo "Connect:email-smtp.us-west-2.amazonaws.com RELAY" >> /etc/mail/access
# Regenerate /etc/mail/access.db
RUN rm /etc/mail/access.db && makemap hash /etc/mail/access.db < /etc/mail/access
# Save a back-up copy of /etc/mail/sendmail.mc and /etc/mail/sendmail.cf.
RUN cp /etc/mail/sendmail.mc /etc/mail/sendmail.mc.old
RUN cp /etc/mail/sendmail.cf /etc/mail/sendmail.cf.old
# Update /etc/mail/sendmail.mc file with AWS Region info
COPY sendmail/sendmail.mc /etc/mail/sendmail.mc
RUN  chmod 666 /etc/mail/sendmail.cf
RUN  m4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf
RUN  chmod 644 /etc/mail/sendmail.cf

# Checkout rhubarb-farmer and set permissions
WORKDIR /home/kbatch/rhubarb-farmer
RUN git clone "https://bitbucket.org/uits-ksd/rhubarb-farmer" . && \
    chown -R kbatch:kuali /home/kbatch/rhubarb-farmer

# Copy in DocuWare script and set permissions
RUN cd /home/pssys/ && \
  mkdir bin && \
  chown pssys:kuali /home/pssys/bin
COPY bin/dspace.sh /home/pssys/bin
RUN chmod u+x /home/pssys/bin/dspace.sh && \
  chown pssys:kuali /home/pssys/bin/dspace.sh

# Add status file cleanup script to daily cron
RUN chmod 744 /usr/local/bin/statusFileCleanup.sh
ADD cron/statusFileCleanup /etc/cron.d/statusFileCleanup
RUN chmod 644 /etc/cron.d/statusFileCleanup

# Add log file cleanup script to daily cron
RUN chmod 744 /usr/local/bin/logFileCleanup.sh
ADD cron/logFileCleanup /etc/cron.d/logFileCleanup
RUN chmod 644 /etc/cron.d/logFileCleanup

# Make sure timezone is Arizona time
RUN yum install -y tzdata
ENV TZ=America/Phoenix

ENV SMTP_SECURITY_DIRECTORY=/security/smtp

# create templates directory
ENV TEMPLATES_DIR="/opt/kuali/run/templates"
RUN mkdir -p /opt/kuali/run/templates
COPY templates /opt/kuali/run/templates

# Install AWS CLI for Control-M to run commands during batch
RUN yum install -y awscli net-tools

ENTRYPOINT /usr/local/bin/rhubarb-server-start.sh
