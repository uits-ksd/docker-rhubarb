# University of Arizona Kuali Rhubarb Docker Container
---

### Description
This project defines an image that will set up Rhubarb scripts and the related configuration files inside a Docker container. It also creates symlinks to the various KFS directories on the mounted EFS volume.

### Security Note
In order to ensure consistent connections into the Rhubarb container (for example, by Control-M), host SSH fingerprints will need to be included in the Docker container. See the fingerprints section below for more details.

### Building
#### Local Testing
A sample build command is: `docker build -t kuali/rhubarb:local7 --build-arg DOCKER_REPOSITORY=kuali/rhubarb --build-arg BASE_DOCKER_TAG_NAME=rhubarb-base-nonprod --build-arg TARGET_ENV=local7 --build-arg GIT_BRANCH=master --force-rm .`

This command will build a **kuali/rhubarb** image and tag it with the name local7.

1. This assumes an image from the **docker-rhubarb-base** code exists and that there is one tagged as **rhubarb-base-nonprod**.
2. For local testing, you should comment out all of the lines which source scripts at the top of rhubarb-server-start.sh, as those assume you are running in AWS.
3. Establish a connection to AWS:
* Use the "AWS Temp Credentials" process to establish connectivity to our KFS non-prod account from your local machine.
* Run this command to log into ECR: `aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin 397167497055.dkr.ecr.us-west-2.amazonaws.com`
4. Run a command like this for a local build: `docker build -t kuali/rhubarb:local7 --build-arg DOCKER_REPOSITORY=397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/rhubarb --build-arg BASE_DOCKER_TAG_NAME=rhubarb-base-nonprod-2024-05-14 --build-arg TARGET_ENV=local7 --build-arg GIT_BRANCH=development --force-rm .` (Assuming you use the latest base image tag and the appropriate GIT_BRANCH of the rhubarb code.)
* If using a Mac, you may need to add `--platform=linux/amd64` to account for the processor (i.e. it is not arm64).

Related to the setup_ssh_fingerprints.sh script - previously, you needed to create a fake fingerprints.tar.gz file so the Docker image will build successfully. Example:
```
$ cd ssh/
ssh $ mkdir fingerprints
ssh $ cd fingerprints
fingerprints $ touch test_key
fingerprints $ touch test_key.pub
fingerprints $ ls -al
total 0
drwxr-xr-x  4 hlo  staff  128 Dec 16 15:27 .
drwxr-xr-x  6 hlo  staff  192 Dec 16 15:27 ..
-rw-r--r--  1 hlo  staff    0 Dec 16 15:27 test_key
-rw-r--r--  1 hlo  staff    0 Dec 16 15:27 test_key.pub
fingerprints $ cd ..
ssh $ tar cvzf fingerprints.tar.gz fingerprints
a fingerprints
a fingerprints/test_key
a fingerprints/test_key.pub
ssh $ ls -al
total 24
drwxr-xr-x   6 hlo  staff  192 Dec 16 15:30 .
drwxr-xr-x  11 hlo  staff  352 Sep  8 16:45 ..
drwxr-xr-x   4 hlo  staff  128 Dec 16 15:27 fingerprints
-rw-r--r--   1 hlo  staff  170 Dec 16 15:30 fingerprints.tar.gz
-rw-r--r--   1 hlo  staff  154 Jun 21  2019 placeholder.txt
```
3. Other setup scripts may also need to be commented out, due to the assumed mounted EFS volume.

#### For An Environment
Utilizing tags is a way for us to build specific images for an environment. This applies to the base image and to the Rhubarb image we create. For consistency and accuracy, we must make sure we have the correct SSH keys; we need to make sure there is a base image of **docker-rhubarb-base** that is tagged for an environment (called **rhubarb-base-nonprod** or **rhubarb-base-prd** ). Then we can build a Rhubarb image.

Example command for the STG environment:

       docker build -t 397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/rhubarb:stg7-release64 --build-arg DOCKER_REPOSITORY=397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/rhubarb --build-arg BASE_DOCKER_TAG_NAME=rhubarb-base-nonprod-2024-05-14 --build-arg TARGET_ENV=sup --build-arg GIT_BRANCH=master --force-rm .

1. This assumes there is a **kuali/rhubarb** image tagged with "rhubarb-base-nonprod-2024-05-14".
2. If a TARGET_ENV build argument is not passed in, the default will be dev7. The value is used for RHUBARB_ENV for related rhubarb configuration (which corresponds to the environment's email config file used as well as what is used in email subjects).
3. Valid TARGET_ENV values:  *local7* (will be treated as dev7), *dev7*, *tst7*, *stg7*, *sup*, *prd*
4. If a GIT_BRANCH build argument is not passed in, the default will be development. The value corresponds to the branch of Rhubarb and Rhubarb configuration we want to use for the build.
5. Valid GIT_BRANCH values: *nameOfAFeatureBranch*, *development*, *master*

### Running
A sample run command (used for testing) is: `docker run -d --privileged --name=rhubarb kuali/rhubarb:local7`

This command will run a container called rhubarb based on the **kuali/rhubarb:local7** image.

If using a Mac, you may need to add this to account for the processor (i.e. it is not arm64): `--platform=linux/amd64`.

If on an EC2 instance, a sample run command is: `sudo docker run -d --privileged --name=rhubarb --hostname=kfs7-stg1 -v /efs/kfs/stg/transactional/:/transactional -p 0.0.0.0:2022:22 397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/rhubarb:stg7`.

This command will run a rhubarb container that has the EFS file mount and the port forwarding for SSH access into the container. Option details:

* `--hostname=` will name the .runnable file (which is used with the Rhubarb and RemoteBatchJobInvoker Java code running in the KFS container)
* `-v /efs/kfs/stg/transactional:/transactional` mounts the STG transactional directory on the EFS for KFS batch files
* `-p 2022:22` maps port 22 in the container to port 2022 on the EC2 host

### Testing
If testing the Rhubarb container from within an EC2 prototype environment to run a batch job as the kbatch user, you will need to add a private key for kbatch to the EC2 container and then change the file permissions to 600. Then you can log into the Docker container with a command like `ssh -v -p 2022 -i /home/ec2-user/.ssh/kbatch kbatch@localhost`. This private key is stored in stache in an entry called "kbatch Private SSH Key For Rhubarb Docker Container".

*Note*: The corresponding public key is stored in the docker-rhubarb-base project in the corresponding ssh environment folder. More details are in the project (https://bitbucket.org/uits-ksd/docker-rhubarb-base).

If testing Rhubarb scripts from outside of the EC2 instance, you can SSH into the Rhubarb container using these steps:
1. Make sure you have the non-prod kbatch private key on your machine. It is located in the "kbatch Private SSH Key For Rhubarb Docker Container" stache entry. The file permissions need to be read and write by owner (600).
2. In the AWS management console, in the Route 53 service, get the IP address for the A Record that is in the kuali.local hosted zone which matches the environment you are testing.
3. You can then log in to the running rhubarb container using a command like this: `ssh -i ~/.ssh/kbatch kbatch@10.220.176.207 -p 2022`.
* Note that the .runnable file is now named something like this `ip-10-220-176-164.us-west-2.compute.internal.runnable` (instead of kfs7-dev.runnable when we had consistent naming using OpsWorks). This corresponds to the IP address of the ECS task that is running KFS and Rice tasks in the batch cluster.

To test Rhubarb scripts, you can use commands similar to what we have configured in Control-M jobs. For example, when logged into a Rhubarb container as kbatch, you can run the commands to initialize a log and run a batch job:
```
[kbatch@ip-10-220-176-207 ~]$ ls -l /transactional/work/logs/BEGIN/
total 4
-rw-r--r-- 1 systemd-oom kfs-writers 3732 Mar 27  2024 BEGIN_2024-03-26.log

[kbatch@ip-10-220-176-207 ~]$ batch_log initialize BEGIN

[kbatch@ip-10-220-176-207 ~]$ ls -l /transactional/work/logs/BEGIN*
-rw-r--r-- 1 kbatch kfs-writers   70 Oct  3 11:45 /transactional/work/logs/BEGIN.log

/transactional/work/logs/BEGIN:
total 4
-rw-r--r-- 1 systemd-oom kfs-writers 3732 Mar 27  2024 BEGIN_2024-03-26.log

[kbatch@ip-10-220-176-207 ~]$ cat /transactional/work/logs/BEGIN.log
11:45:42 (INFO) initialize
Thu, 03 Oct 2024 11:45:42 -0700 initialize

[kbatch@ip-10-220-176-207 ~]$ batch_drive BEGIN clearCacheJob
2024-10-03 12:12:14 (INFO) Waiting for "/transactional/work/control/clearCacheJob_20241003121214.run"
2024-10-03 12:12:14 (INFO) Timeout is 2024-10-03 15:42:14 -0700 (12600 seconds from now).
2024-10-03 12:13:14 (INFO) Statusfile found: /transactional/work/control/history/clearCacheJob_20241003121214.status
2024-10-03 12:13:14 (INFO) clearCacheJob succeeded:
2024-10-03 12:13:14 (INFO) > [2024/10/03 12:12:22] Succeeded
```

### Additional Technical Details

##### Rhubarb Configuration
Currently the TARGET_ENV build argument must match one of the environment's deployment files in config/deploy in both katt-kfs-rhubarb and katt-kfs-rhubarb-config projects. It corresponds to the desired environment where Rhubarb will be running.

The GIT_BRANCH build argument must match a branch in the katt-kfs-rhubarb and katt-kfs-rhubarb-config projects.

##### Fingerprints
In order for Control-M to be able to consistently connect to an AWS environment running KFS and Rhubarb, we are utilizing a fingerprints archive file that is copied from an S3 bucket in the AWS account where the environment will be deployed during container start-up. (Before we migrated away from OpsWorks, the file was pulled into the build workspace and utilized in the Rhubarb Docker image at build time.)

The current fingerprints.tar.gz file came from an AWS prototype environment that was running the KFS and Rhubarb Docker containers. The public and private keys were compressed and then uploaded to the docker-rhubarb folder in the kfs-cloudformation-deployment S3 bucket in our Kuali non-production AWS account.

##### Rhubarb Farmer
Shell script to run KFS batch jobs using Rhubarb, but on the command line. See https://bitbucket.org/uits-ksd/rhubarb-farmer.

##### Sendmail
Allows us to send emails using batch_deliver on our AWS environments. SES credentials are populated at runtime.

##### DocuWare
We moved our DocuWare script out of a Control-M job and into the Rhubarb Docker container. During testing, we encountered errors with the job that wrote the script to the app server on a regular basis (UAF-DOCUWR-INI-SCRIPT), so this is the workaround - to include the script in the Rhubarb container so that the pssys user would always have it.

### Next Steps / Improvements
1. Update the hard-coded host in sendmail.mc for DR purposes.