# University of Arizona Kuali Rhubarb Docker Container - cron
---

## Description
To automate tasks associated with Rhubarb, we are utilizing cron.

## Running
Assuming the Rhubarb Docker container is running, the defined tasks will be executed.

## Current Tasks
### Status File Cleanup
#### Environment Variables
We assume these the EFS volume is mounted in the container. This should be defined in the template that controls the Rhubarb container. The clean-up script has a default file path that assumes `/transactional/work/control/history/` exists. Example:
```
- Key: docker_volumes
  Value: !Sub "/efs/kfs/${EnvSlug}/transactional:/transactional,/kuali-configs/security/smtp:/rhubarb-security:ro,/efs/kfs/${EnvSlug}/integration:/integration"
```

#### Script Parameters
1. CLEANUP_FILE_PATH: The local path to files that need to be deleted. Our default value is `/transactional/work/control/history/`.
2. DAYS_TO_KEEP: If provided, will define the max age of the files to keep. Our default value is 7 days.

*Note 1:* The script assumes the arguments are passed into the script in the above order.
*Note 2:* We should only pass in arguments if we want to override the default values and run the script manually.
*Note 3:* The crontab entry is not defined with any arguments for this script.

#### Running
As defined by the crontab entry (cron.d/statusFileCleanup), the `statusFileCleanup.sh` bash script will run against the defined directory, deleting all files except the most recent 7 days worth from /transactional/work/control/history/. These files are generated whenever Rhubarb kicks off a KFS batch job, and are only useful if troubleshooting a batch failure within 24 hours of the incident.

#### Testing
Testing was done in a local Docker container using the bin/createTestStatusFiles.sh bash script to generate test status files. (Renamed this script to createTestFiles.sh when the logFileCleanup.sh script was added to this project.) The crontab entry was manually modified so that file cleanup could be confirmed within a few minutes.

Example commands and output:
```
bash-4.2# bash /usr/local/bin/createTestStatusFiles.sh /transactional/work/control/history/ clearCacheJob

bash-4.2#  ls -al /transactional/work/control/history/
total 8
drwxrwxr-x 2 root root 4096 Jan 12 18:50 .
drwxrwxr-x 3 root root 4096 Jan 12 18:50 ..
-rw-rw-r-- 1 root root    0 Jan  1 00:00 clearCacheJob01.status
-rw-rw-r-- 1 root root    0 Jan  2 00:00 clearCacheJob02.status
-rw-rw-r-- 1 root root    0 Jan  3 00:00 clearCacheJob03.status
-rw-rw-r-- 1 root root    0 Jan  4 00:00 clearCacheJob04.status
-rw-rw-r-- 1 root root    0 Jan  5 00:00 clearCacheJob05.status
-rw-rw-r-- 1 root root    0 Jan  6 00:00 clearCacheJob06.status
-rw-rw-r-- 1 root root    0 Jan  7 00:00 clearCacheJob07.status
-rw-rw-r-- 1 root root    0 Jan  8 00:00 clearCacheJob08.status
-rw-rw-r-- 1 root root    0 Jan  9 00:00 clearCacheJob09.status
-rw-rw-r-- 1 root root    0 Jan 10 00:00 clearCacheJob10.status
-rw-rw-r-- 1 root root    0 Jan 11 00:00 clearCacheJob11.status
-rw-rw-r-- 1 root root    0 Jan 12 00:00 clearCacheJob12.status
bash-4.2# 
bash-4.2# vi /etc/cron.d/statusFileCleanup 

bash-4.2# cat /var/log/fileCleanup-01-12-2021.log 
You provided the arguments:
No file path supplied; using default of /transactional/work/control/history/
No day amount supplied; using default of 7
=============
Start file cleanup at Tue Jan 12 23:01:01 UTC 2021
=============

Files To Delete:
2623865    0 -rw-rw-r--   1 root     root            0 Jan  4 07:00 /transactional/work/control/history/clearCacheJob04.status
2623863    0 -rw-rw-r--   1 root     root            0 Jan  2 07:00 /transactional/work/control/history/clearCacheJob02.status
2623864    0 -rw-rw-r--   1 root     root            0 Jan  3 07:00 /transactional/work/control/history/clearCacheJob03.status
2623862    0 -rw-rw-r--   1 root     root            0 Jan  1 07:00 /transactional/work/control/history/clearCacheJob01.status

Now deleting the files.

=============
End file cleanup at Tue Jan 12 23:01:01 UTC 2021
=============
bash-4.2# 
bash-4.2# 
bash-4.2# ls -al /transactional/work/control/history/
total 8
drwxrwxr-x 2 root root 4096 Jan 12 16:01 .
drwxrwxr-x 3 root root 4096 Jan 12 15:59 ..
-rw-rw-r-- 1 root root    0 Jan  5 00:00 clearCacheJob05.status
-rw-rw-r-- 1 root root    0 Jan  6 00:00 clearCacheJob06.status
-rw-rw-r-- 1 root root    0 Jan  7 00:00 clearCacheJob07.status
-rw-rw-r-- 1 root root    0 Jan  8 00:00 clearCacheJob08.status
-rw-rw-r-- 1 root root    0 Jan  9 00:00 clearCacheJob09.status
-rw-rw-r-- 1 root root    0 Jan 10 00:00 clearCacheJob10.status
-rw-rw-r-- 1 root root    0 Jan 11 00:00 clearCacheJob11.status
-rw-rw-r-- 1 root root    0 Jan 12 00:00 clearCacheJob12.status
bash-4.2# exit
```

### Log File Cleanup
This does the same thing as the status file cleanup but will delete the logs that correspond to Control-M folder and job names.

It uses the hard-coded maxdepth value of 2 in the find command, as the log files are in folders in `/transactional/work/logs/`.

#### Script Parameters
1. CLEANUP_FILE_PATH: The local path to files that need to be deleted. Our default value is `/transactional/work/logs/`.
2. DAYS_TO_KEEP: If provided, will define the max age of the files to keep. Our default value is 3 days (will cover a weekend of batch jobs).

#### Testing
Testing was done in a local Docker container using the bin/createTestFiles.sh script to generate test files in a couple of directories, and then logFileCleanup.sh was run for manual testing.

Example commands and output:
```
sh-5.2# cd /usr/local/bin/
sh-5.2# ./createTestFiles.sh /transactional/work/logs/CLRCACHE CLRCACHE
sh-5.2# ls -al /transactional/work/logs/CLRCACHE/
total 0
drwxr-xr-x 28 root root 896 Nov 26 11:55 .
drwxr-xr-x  3 root root  96 Nov 26 11:55 ..
-rw-r--r--  1 root root   0 Nov  1 00:00 CLRCACHE01.status
-rw-r--r--  1 root root   0 Nov  2 00:00 CLRCACHE02.status
-rw-r--r--  1 root root   0 Nov  3 00:00 CLRCACHE03.status
-rw-r--r--  1 root root   0 Nov  4 00:00 CLRCACHE04.status
-rw-r--r--  1 root root   0 Nov  5 00:00 CLRCACHE05.status
-rw-r--r--  1 root root   0 Nov  6 00:00 CLRCACHE06.status
-rw-r--r--  1 root root   0 Nov  7 00:00 CLRCACHE07.status
-rw-r--r--  1 root root   0 Nov  8 00:00 CLRCACHE08.status
-rw-r--r--  1 root root   0 Nov  9 00:00 CLRCACHE09.status
-rw-r--r--  1 root root   0 Nov 10 00:00 CLRCACHE10.status
-rw-r--r--  1 root root   0 Nov 11 00:00 CLRCACHE11.status
-rw-r--r--  1 root root   0 Nov 12 00:00 CLRCACHE12.status
-rw-r--r--  1 root root   0 Nov 13 00:00 CLRCACHE13.status
-rw-r--r--  1 root root   0 Nov 14 00:00 CLRCACHE14.status
-rw-r--r--  1 root root   0 Nov 15 00:00 CLRCACHE15.status
-rw-r--r--  1 root root   0 Nov 16 00:00 CLRCACHE16.status
-rw-r--r--  1 root root   0 Nov 17 00:00 CLRCACHE17.status
-rw-r--r--  1 root root   0 Nov 18 00:00 CLRCACHE18.status
-rw-r--r--  1 root root   0 Nov 19 00:00 CLRCACHE19.status
-rw-r--r--  1 root root   0 Nov 20 00:00 CLRCACHE20.status
-rw-r--r--  1 root root   0 Nov 21 00:00 CLRCACHE21.status
-rw-r--r--  1 root root   0 Nov 22 00:00 CLRCACHE22.status
-rw-r--r--  1 root root   0 Nov 23 00:00 CLRCACHE23.status
-rw-r--r--  1 root root   0 Nov 24 00:00 CLRCACHE24.status
-rw-r--r--  1 root root   0 Nov 25 00:00 CLRCACHE25.status
-rw-r--r--  1 root root   0 Nov 26 00:00 CLRCACHE26.status
sh-5.2# ./createTestFiles.sh /transactional/work/logs/BEGIN BEGIN      
sh-5.2# ls -al /transactional/work/logs/BEGIN/   
total 0
drwxr-xr-x 28 root root 896 Nov 26 11:55 .
drwxr-xr-x  4 root root 128 Nov 26 11:55 ..
-rw-r--r--  1 root root   0 Nov  1 00:00 BEGIN01.status
-rw-r--r--  1 root root   0 Nov  2 00:00 BEGIN02.status
-rw-r--r--  1 root root   0 Nov  3 00:00 BEGIN03.status
-rw-r--r--  1 root root   0 Nov  4 00:00 BEGIN04.status
-rw-r--r--  1 root root   0 Nov  5 00:00 BEGIN05.status
-rw-r--r--  1 root root   0 Nov  6 00:00 BEGIN06.status
-rw-r--r--  1 root root   0 Nov  7 00:00 BEGIN07.status
-rw-r--r--  1 root root   0 Nov  8 00:00 BEGIN08.status
-rw-r--r--  1 root root   0 Nov  9 00:00 BEGIN09.status
-rw-r--r--  1 root root   0 Nov 10 00:00 BEGIN10.status
-rw-r--r--  1 root root   0 Nov 11 00:00 BEGIN11.status
-rw-r--r--  1 root root   0 Nov 12 00:00 BEGIN12.status
-rw-r--r--  1 root root   0 Nov 13 00:00 BEGIN13.status
-rw-r--r--  1 root root   0 Nov 14 00:00 BEGIN14.status
-rw-r--r--  1 root root   0 Nov 15 00:00 BEGIN15.status
-rw-r--r--  1 root root   0 Nov 16 00:00 BEGIN16.status
-rw-r--r--  1 root root   0 Nov 17 00:00 BEGIN17.status
-rw-r--r--  1 root root   0 Nov 18 00:00 BEGIN18.status
-rw-r--r--  1 root root   0 Nov 19 00:00 BEGIN19.status
-rw-r--r--  1 root root   0 Nov 20 00:00 BEGIN20.status
-rw-r--r--  1 root root   0 Nov 21 00:00 BEGIN21.status
-rw-r--r--  1 root root   0 Nov 22 00:00 BEGIN22.status
-rw-r--r--  1 root root   0 Nov 23 00:00 BEGIN23.status
-rw-r--r--  1 root root   0 Nov 24 00:00 BEGIN24.status
-rw-r--r--  1 root root   0 Nov 25 00:00 BEGIN25.status
-rw-r--r--  1 root root   0 Nov 26 00:00 BEGIN26.status
sh-5.2# 

sh-5.2# ./logFileCleanup.sh 
You provided the arguments:
No file path supplied; using default of /transactional/work/logs/
No day amount supplied; using default of 3
=============
Start file cleanup at Tue Nov 26 13:25:14 MST 2024
=============

Files To Delete:
        7      0 -rw-r--r--   1 root     root            0 Nov 21 00:00 /transactional/work/logs/BEGIN/BEGIN21.status
        8      0 -rw-r--r--   1 root     root            0 Nov  9 00:00 /transactional/work/logs/BEGIN/BEGIN09.status
        9      0 -rw-r--r--   1 root     root            0 Nov 17 00:00 /transactional/work/logs/BEGIN/BEGIN17.status
       10      0 -rw-r--r--   1 root     root            0 Nov  5 00:00 /transactional/work/logs/BEGIN/BEGIN05.status
       12      0 -rw-r--r--   1 root     root            0 Nov  7 00:00 /transactional/work/logs/BEGIN/BEGIN07.status
       13      0 -rw-r--r--   1 root     root            0 Nov 19 00:00 /transactional/work/logs/BEGIN/BEGIN19.status
       14      0 -rw-r--r--   1 root     root            0 Nov 15 00:00 /transactional/work/logs/BEGIN/BEGIN15.status
       15      0 -rw-r--r--   1 root     root            0 Nov 11 00:00 /transactional/work/logs/BEGIN/BEGIN11.status
       16      0 -rw-r--r--   1 root     root            0 Nov  3 00:00 /transactional/work/logs/BEGIN/BEGIN03.status
       17      0 -rw-r--r--   1 root     root            0 Nov  1 00:00 /transactional/work/logs/BEGIN/BEGIN01.status
       18      0 -rw-r--r--   1 root     root            0 Nov 13 00:00 /transactional/work/logs/BEGIN/BEGIN13.status
       20      0 -rw-r--r--   1 root     root            0 Nov 16 00:00 /transactional/work/logs/BEGIN/BEGIN16.status
       21      0 -rw-r--r--   1 root     root            0 Nov  8 00:00 /transactional/work/logs/BEGIN/BEGIN08.status
       22      0 -rw-r--r--   1 root     root            0 Nov  4 00:00 /transactional/work/logs/BEGIN/BEGIN04.status
       23      0 -rw-r--r--   1 root     root            0 Nov 20 00:00 /transactional/work/logs/BEGIN/BEGIN20.status
       24      0 -rw-r--r--   1 root     root            0 Nov 18 00:00 /transactional/work/logs/BEGIN/BEGIN18.status
       25      0 -rw-r--r--   1 root     root            0 Nov  6 00:00 /transactional/work/logs/BEGIN/BEGIN06.status
       26      0 -rw-r--r--   1 root     root            0 Nov 14 00:00 /transactional/work/logs/BEGIN/BEGIN14.status
       27      0 -rw-r--r--   1 root     root            0 Nov 22 00:00 /transactional/work/logs/BEGIN/BEGIN22.status
       29      0 -rw-r--r--   1 root     root            0 Nov 10 00:00 /transactional/work/logs/BEGIN/BEGIN10.status
       30      0 -rw-r--r--   1 root     root            0 Nov  2 00:00 /transactional/work/logs/BEGIN/BEGIN02.status
       32      0 -rw-r--r--   1 root     root            0 Nov 12 00:00 /transactional/work/logs/BEGIN/BEGIN12.status
       33      0 -rw-r--r--   1 root     root            0 Nov  5 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE05.status
       34      0 -rw-r--r--   1 root     root            0 Nov  9 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE09.status
       35      0 -rw-r--r--   1 root     root            0 Nov 17 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE17.status
       36      0 -rw-r--r--   1 root     root            0 Nov 21 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE21.status
       37      0 -rw-r--r--   1 root     root            0 Nov 15 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE15.status
       38      0 -rw-r--r--   1 root     root            0 Nov  7 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE07.status
       39      0 -rw-r--r--   1 root     root            0 Nov 19 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE19.status
       41      0 -rw-r--r--   1 root     root            0 Nov  3 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE03.status
       42      0 -rw-r--r--   1 root     root            0 Nov 11 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE11.status
       44      0 -rw-r--r--   1 root     root            0 Nov 13 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE13.status
       45      0 -rw-r--r--   1 root     root            0 Nov  1 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE01.status
       46      0 -rw-r--r--   1 root     root            0 Nov 20 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE20.status
       47      0 -rw-r--r--   1 root     root            0 Nov  4 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE04.status
       48      0 -rw-r--r--   1 root     root            0 Nov 16 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE16.status
       49      0 -rw-r--r--   1 root     root            0 Nov  8 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE08.status
       50      0 -rw-r--r--   1 root     root            0 Nov 22 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE22.status
       51      0 -rw-r--r--   1 root     root            0 Nov 14 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE14.status
       52      0 -rw-r--r--   1 root     root            0 Nov 18 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE18.status
       53      0 -rw-r--r--   1 root     root            0 Nov  6 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE06.status
       54      0 -rw-r--r--   1 root     root            0 Nov  2 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE02.status
       55      0 -rw-r--r--   1 root     root            0 Nov 10 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE10.status
       57      0 -rw-r--r--   1 root     root            0 Nov 12 00:00 /transactional/work/logs/CLRCACHE/CLRCACHE12.status

Now deleting the files.

=============
End file cleanup at Tue Nov 26 13:25:14 MST 2024
=============
sh-5.2# 

sh-5.2# ls -al /transactional/work/logs/CLRCACHE/
total 0
drwxr-xr-x 6 root root 192 Nov 26 13:25 .
drwxr-xr-x 4 root root 128 Nov 26 11:55 ..
-rw-r--r-- 1 root root   0 Nov 23 00:00 CLRCACHE23.status
-rw-r--r-- 1 root root   0 Nov 24 00:00 CLRCACHE24.status
-rw-r--r-- 1 root root   0 Nov 25 00:00 CLRCACHE25.status
-rw-r--r-- 1 root root   0 Nov 26 00:00 CLRCACHE26.status
sh-5.2# 
sh-5.2# ls -al /transactional/work/logs/BEGIN/   
total 0
drwxr-xr-x 6 root root 192 Nov 26 13:25 .
drwxr-xr-x 4 root root 128 Nov 26 11:55 ..
-rw-r--r-- 1 root root   0 Nov 23 00:00 BEGIN23.status
-rw-r--r-- 1 root root   0 Nov 24 00:00 BEGIN24.status
-rw-r--r-- 1 root root   0 Nov 25 00:00 BEGIN25.status
-rw-r--r-- 1 root root   0 Nov 26 00:00 BEGIN26.status
sh-5.2# 
```

## Dependencies
This assumes that the OS (defined in the base image) has cron installed. It is started with the `rhubarb-server-start.sh` ENTRYPOINT script. Currently this is based on Amazon Linux 2023.

It also assumes the Arizona timezone, set with the tzdata package and an environment variable in the Dockerfile. Commands adapted from https://medium.com/rahasak/configure-docker-timezone-in-linux-71893fe51499.
